import psycopg2
import smtplib
import paho.mqtt.client as mqtt
import datetime
import requests
import json

verbose = True

def send_email(user, pwd, recipient, subject, body):
    import smtplib

    gmail_user = user
    gmail_pwd = pwd
    FROM = user
    TO = recipient if type(recipient) is list else [recipient]
    SUBJECT = subject
    TEXT = body

    # Prepare actual message
    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(FROM, TO, message)
        server.close()
	if verbose:
	        print 'successfully sent the mail'
    except:
	if verbose:
	        print "failed to send mail"

def time_now():
    return datetime.datetime.now().strftime('%H:%M:%S.%f')

# MQTT client to connect to the bus
mqtt_client = mqtt.Client()

def on_connect(client, userdata, flags, rc):
    # subscribe to all messages
    mqtt_client.subscribe('#')

# Process a message as it arrives
def on_message(client, userdata, msg):
        
	if verbose:
		print('Message topic: {}'.format(msg.topic))
	
	if (msg.topic == 'hermes/intent/user_erDmAevwO__StepsCountIntent'):
                if verbose:
                        print('Do count steps')                
		
		# Connect to an existing database
		conn = psycopg2.connect("dbname='genuino101' user='pi' host='localhost' password='ma121284'")

		# Open a cursor to perform database operations
		cur = conn.cursor()

		# Query the database and obtain data as Python objects
		cur.execute("SELECT stepcount FROM steps ORDER BY ts DESC LIMIT 1;")
		stepCount = cur.fetchone()[0]
		if verbose:
			print("Fetched timesteps value: " + str(stepCount))

		# Make the changes to the database persistent
		conn.commit()

		# Close communication with the database
		cur.close()
		conn.close()

		say("Counted {} steps.".format(stepCount))

	elif (msg.topic == 'hermes/intent/user_erDmAevwO__EmailFamilyIntent'):
		if verbose:
			print('Do email family')
		say("Emailing family.")
		send_email('elderlyassistanttest', '181f3a199898', 'madks@hotmail.com','I fell down. Help is needed', 'Hello family members. An automated system detected that I fell down. Help is needed.')
	elif (msg.topic == 'hermes/intent/user_erDmAevwO__DirtyPotteryIntent'):
		if verbose:
			print('Returns dirty cup count.')
		r = requests.get("http://tegra-ubuntu:5000/dishes")
		print("Got: {} dirty cups.".format(int(r.json()["data"][0]["qty"])))
		if verbose:
                        print(r.status_code, r.reason)		
		say("Got: {} dirty cups.".format(int(r.json()["data"][0]["qty"])))
	elif (msg.topic == 'hermes/intent/user_erDmAevwO__TurnOnLights'):
                if verbose:
                        print('Turns on office light.')
		say("Turning on office light.")
		r = requests.put("http://philips-hue/api/xjLn9sej21hdRIXl0PrwVanhCEg7H7bGIqD5APd2/lights/2/state", data='{"on": true}')
		if verbose:
                        print(r.status_code, r.reason)
	elif (msg.topic == 'hermes/intent/user_erDmAevwO__TurnOffLightsIntent'):
                if verbose:
                        print('Turns off office light.')
		say("Turning off office light")
		r = requests.put("http://philips-hue/api/xjLn9sej21hdRIXl0PrwVanhCEg7H7bGIqD5APd2/lights/2/state", data='{"on": false }')
		if verbose:
                	print(r.status_code, r.reason)
        elif (msg.topic == 'hermes/intent/user_erDmAevwO__GetTemperatureIntent'):
                if verbose:
                        print('Getting temperature from HA.')
		r = requests.get("http://ha:8123/api/states/sensor.dht22_temperature")
		temperature = r.json()["state"]
                say("Temperature in the house is " + temperature + " degrees celsius.")
                if verbose:
                        print(r.status_code, r.reason)
        elif (msg.topic == 'hermes/intent/user_erDmAevwO__GetHumidityIntent'):
                if verbose:
                        print('Getting humidity from HA.')
		r = requests.get("http://ha:8123/api/states/sensor.dht22_humidity")
		humidity = r.json()["state"]
                say("Humidity in the house is: " + humidity + " percent.")                
                if verbose:
                        print(r.status_code, r.reason)
        elif (msg.topic == 'hermes/intent/user_erDmAevwO__GetPressureIntent'):
                if verbose:
                        print('Getting pressure from ESP8266.')
                r = requests.get("http://192.168.0.139/variables/pressure")
                pressure = r.json()["pressure"]
                say("Barometric pressure in the house is: " + str(pressure) + " kilo pascal.")
                if verbose:
                        print(r.status_code, r.reason)




def say(text):
    '''
    Print the output to the console and to the TTS engine
    '''
    if verbose:
	print(text)
    mqtt_client.publish('hermes/tts/say', json.dumps({'text': text}))
	
mqtt_client.on_connect = on_connect
mqtt_client.on_message = on_message
mqtt_client.connect('localhost', 9898)
mqtt_client.loop_forever()
